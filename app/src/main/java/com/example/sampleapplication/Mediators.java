package com.example.sampleapplication;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Mediators {
    @Id(autoincrement = true)
    Long id;

    String source;

    @Generated(hash = 1436897860)
    public Mediators(Long id, String source) {
        this.id = id;
        this.source = source;
    }

    @Generated(hash = 1464453117)
    public Mediators() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
