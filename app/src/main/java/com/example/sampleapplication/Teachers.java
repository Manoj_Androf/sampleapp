package com.example.sampleapplication;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Teachers {

    @Id(autoincrement = true)
    Long id;

    String name;

    @Generated(hash = 1416998821)
    public Teachers(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 807896100)
    public Teachers() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
