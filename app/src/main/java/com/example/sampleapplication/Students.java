package com.example.sampleapplication;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Students {

    @org.greenrobot.greendao.annotation.Id(autoincrement = true)
    Long Id;

    String sample;

    @Generated(hash = 782777115)
    public Students(Long Id, String sample) {
        this.Id = Id;
        this.sample = sample;
    }

    @Generated(hash = 174834727)
    public Students() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getSample() {
        return this.sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }
}
