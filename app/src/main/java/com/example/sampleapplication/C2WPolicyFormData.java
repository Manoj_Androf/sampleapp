package com.example.sampleapplication;
import android.os.Parcelable;
import kotlinx.android.parcel.Parcelize;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Manoj on 2019-06-27.
 */

/**
 * Entity mapped to table "C2WPolicyFormData".
 */
@Entity
public class C2WPolicyFormData {

    private static final long serialVersionUID = 1L;
    @Id(autoincrement = true)
    private Long id;

    private String lead_number;
    private String product_code;
    private String quote_id;
    /**
     * Insured Basic Details
     */

    private String email;
    private String phone;
    private String dob;
    private String first_name;
    private String last_name;
    private String country;
    private String countryCode;
    private String gender;
    private String is_nri;
    private String is_smoker;
    private String occupation;
    private String age;
    private String phoneCountryCode;

    /**
     * Proposer Basic Details
     */

    private String proposer_email;
    private String proposer_phone;
    private String proposer_dob;
    private String proposer_first_name;
    private String proposer_last_name;
    private String proposer_country;
    private String proposer_countryCode;
    private String proposer_gender;
    private String proposer_is_nri;
    private String proposer_is_smoker;
    private String proposer_occupation;
    private String proposer_age;
    private String proposer_phoneCountryCode;

    /**
     * Investment Details
     */

    private String investmentGoal;
    private String investingFor;
    private String premiumAmount;
    private String premiumFrequency;
    private String policyTerm;
    private String premiumPayingTerm;
    private Boolean isForWholeLife=true;
    private String totalSumAssured;

    private String planOption;


    private String comboPolicyTerm;
    private String comboPremiumFrequency;
    private String comboPremiumPayingTerm;
    private String comboTotalSumAssured;
    private String comboPremiumAmount;
    private String totalCover;
    private String totalPremium;
    private String insuredType;

    /**
     * Fund Details
     * */
    private String discoveryFund;
    private String opportunitiesFund;
    private String equityPlusFund;
    private String diversifiedEquityFund;
    private String blueChipFund;
    private String balancedFund;
    private String bondFund;
    private String liquidFund;

    private String investmentStyle;

    /**
     * STP Details
    * */
    private String sourceFund;
    private String targetFund;
    private String transferAmt;
    private String transferDate;


    /**
     * Miscellaneous
     * */
    private String agent_code;
    private String source_code;
    private String premiumPayingTermOption;
    private String modifiedDate;
    private String isComboAdded;
    private Boolean isValid;
    private String txn_id;
    private String applicationNumber;
    private String paymentMode;
    @Generated(hash = 1699432041)
    public C2WPolicyFormData(Long id, String lead_number, String product_code,
            String quote_id, String email, String phone, String dob,
            String first_name, String last_name, String country, String countryCode,
            String gender, String is_nri, String is_smoker, String occupation,
            String age, String phoneCountryCode, String proposer_email,
            String proposer_phone, String proposer_dob, String proposer_first_name,
            String proposer_last_name, String proposer_country,
            String proposer_countryCode, String proposer_gender,
            String proposer_is_nri, String proposer_is_smoker,
            String proposer_occupation, String proposer_age,
            String proposer_phoneCountryCode, String investmentGoal,
            String investingFor, String premiumAmount, String premiumFrequency,
            String policyTerm, String premiumPayingTerm, Boolean isForWholeLife,
            String totalSumAssured, String planOption, String comboPolicyTerm,
            String comboPremiumFrequency, String comboPremiumPayingTerm,
            String comboTotalSumAssured, String comboPremiumAmount,
            String totalCover, String totalPremium, String insuredType,
            String discoveryFund, String opportunitiesFund, String equityPlusFund,
            String diversifiedEquityFund, String blueChipFund, String balancedFund,
            String bondFund, String liquidFund, String investmentStyle,
            String sourceFund, String targetFund, String transferAmt,
            String transferDate, String agent_code, String source_code,
            String premiumPayingTermOption, String modifiedDate,
            String isComboAdded, Boolean isValid, String txn_id,
            String applicationNumber, String paymentMode) {
        this.id = id;
        this.lead_number = lead_number;
        this.product_code = product_code;
        this.quote_id = quote_id;
        this.email = email;
        this.phone = phone;
        this.dob = dob;
        this.first_name = first_name;
        this.last_name = last_name;
        this.country = country;
        this.countryCode = countryCode;
        this.gender = gender;
        this.is_nri = is_nri;
        this.is_smoker = is_smoker;
        this.occupation = occupation;
        this.age = age;
        this.phoneCountryCode = phoneCountryCode;
        this.proposer_email = proposer_email;
        this.proposer_phone = proposer_phone;
        this.proposer_dob = proposer_dob;
        this.proposer_first_name = proposer_first_name;
        this.proposer_last_name = proposer_last_name;
        this.proposer_country = proposer_country;
        this.proposer_countryCode = proposer_countryCode;
        this.proposer_gender = proposer_gender;
        this.proposer_is_nri = proposer_is_nri;
        this.proposer_is_smoker = proposer_is_smoker;
        this.proposer_occupation = proposer_occupation;
        this.proposer_age = proposer_age;
        this.proposer_phoneCountryCode = proposer_phoneCountryCode;
        this.investmentGoal = investmentGoal;
        this.investingFor = investingFor;
        this.premiumAmount = premiumAmount;
        this.premiumFrequency = premiumFrequency;
        this.policyTerm = policyTerm;
        this.premiumPayingTerm = premiumPayingTerm;
        this.isForWholeLife = isForWholeLife;
        this.totalSumAssured = totalSumAssured;
        this.planOption = planOption;
        this.comboPolicyTerm = comboPolicyTerm;
        this.comboPremiumFrequency = comboPremiumFrequency;
        this.comboPremiumPayingTerm = comboPremiumPayingTerm;
        this.comboTotalSumAssured = comboTotalSumAssured;
        this.comboPremiumAmount = comboPremiumAmount;
        this.totalCover = totalCover;
        this.totalPremium = totalPremium;
        this.insuredType = insuredType;
        this.discoveryFund = discoveryFund;
        this.opportunitiesFund = opportunitiesFund;
        this.equityPlusFund = equityPlusFund;
        this.diversifiedEquityFund = diversifiedEquityFund;
        this.blueChipFund = blueChipFund;
        this.balancedFund = balancedFund;
        this.bondFund = bondFund;
        this.liquidFund = liquidFund;
        this.investmentStyle = investmentStyle;
        this.sourceFund = sourceFund;
        this.targetFund = targetFund;
        this.transferAmt = transferAmt;
        this.transferDate = transferDate;
        this.agent_code = agent_code;
        this.source_code = source_code;
        this.premiumPayingTermOption = premiumPayingTermOption;
        this.modifiedDate = modifiedDate;
        this.isComboAdded = isComboAdded;
        this.isValid = isValid;
        this.txn_id = txn_id;
        this.applicationNumber = applicationNumber;
        this.paymentMode = paymentMode;
    }
    @Generated(hash = 1426669700)
    public C2WPolicyFormData() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLead_number() {
        return this.lead_number;
    }
    public void setLead_number(String lead_number) {
        this.lead_number = lead_number;
    }
    public String getProduct_code() {
        return this.product_code;
    }
    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }
    public String getQuote_id() {
        return this.quote_id;
    }
    public void setQuote_id(String quote_id) {
        this.quote_id = quote_id;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhone() {
        return this.phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getDob() {
        return this.dob;
    }
    public void setDob(String dob) {
        this.dob = dob;
    }
    public String getFirst_name() {
        return this.first_name;
    }
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }
    public String getLast_name() {
        return this.last_name;
    }
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
    public String getCountry() {
        return this.country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getCountryCode() {
        return this.countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public String getGender() {
        return this.gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getIs_nri() {
        return this.is_nri;
    }
    public void setIs_nri(String is_nri) {
        this.is_nri = is_nri;
    }
    public String getIs_smoker() {
        return this.is_smoker;
    }
    public void setIs_smoker(String is_smoker) {
        this.is_smoker = is_smoker;
    }
    public String getOccupation() {
        return this.occupation;
    }
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    public String getAge() {
        return this.age;
    }
    public void setAge(String age) {
        this.age = age;
    }
    public String getPhoneCountryCode() {
        return this.phoneCountryCode;
    }
    public void setPhoneCountryCode(String phoneCountryCode) {
        this.phoneCountryCode = phoneCountryCode;
    }
    public String getProposer_email() {
        return this.proposer_email;
    }
    public void setProposer_email(String proposer_email) {
        this.proposer_email = proposer_email;
    }
    public String getProposer_phone() {
        return this.proposer_phone;
    }
    public void setProposer_phone(String proposer_phone) {
        this.proposer_phone = proposer_phone;
    }
    public String getProposer_dob() {
        return this.proposer_dob;
    }
    public void setProposer_dob(String proposer_dob) {
        this.proposer_dob = proposer_dob;
    }
    public String getProposer_first_name() {
        return this.proposer_first_name;
    }
    public void setProposer_first_name(String proposer_first_name) {
        this.proposer_first_name = proposer_first_name;
    }
    public String getProposer_last_name() {
        return this.proposer_last_name;
    }
    public void setProposer_last_name(String proposer_last_name) {
        this.proposer_last_name = proposer_last_name;
    }
    public String getProposer_country() {
        return this.proposer_country;
    }
    public void setProposer_country(String proposer_country) {
        this.proposer_country = proposer_country;
    }
    public String getProposer_countryCode() {
        return this.proposer_countryCode;
    }
    public void setProposer_countryCode(String proposer_countryCode) {
        this.proposer_countryCode = proposer_countryCode;
    }
    public String getProposer_gender() {
        return this.proposer_gender;
    }
    public void setProposer_gender(String proposer_gender) {
        this.proposer_gender = proposer_gender;
    }
    public String getProposer_is_nri() {
        return this.proposer_is_nri;
    }
    public void setProposer_is_nri(String proposer_is_nri) {
        this.proposer_is_nri = proposer_is_nri;
    }
    public String getProposer_is_smoker() {
        return this.proposer_is_smoker;
    }
    public void setProposer_is_smoker(String proposer_is_smoker) {
        this.proposer_is_smoker = proposer_is_smoker;
    }
    public String getProposer_occupation() {
        return this.proposer_occupation;
    }
    public void setProposer_occupation(String proposer_occupation) {
        this.proposer_occupation = proposer_occupation;
    }
    public String getProposer_age() {
        return this.proposer_age;
    }
    public void setProposer_age(String proposer_age) {
        this.proposer_age = proposer_age;
    }
    public String getProposer_phoneCountryCode() {
        return this.proposer_phoneCountryCode;
    }
    public void setProposer_phoneCountryCode(String proposer_phoneCountryCode) {
        this.proposer_phoneCountryCode = proposer_phoneCountryCode;
    }
    public String getInvestmentGoal() {
        return this.investmentGoal;
    }
    public void setInvestmentGoal(String investmentGoal) {
        this.investmentGoal = investmentGoal;
    }
    public String getInvestingFor() {
        return this.investingFor;
    }
    public void setInvestingFor(String investingFor) {
        this.investingFor = investingFor;
    }
    public String getPremiumAmount() {
        return this.premiumAmount;
    }
    public void setPremiumAmount(String premiumAmount) {
        this.premiumAmount = premiumAmount;
    }
    public String getPremiumFrequency() {
        return this.premiumFrequency;
    }
    public void setPremiumFrequency(String premiumFrequency) {
        this.premiumFrequency = premiumFrequency;
    }
    public String getPolicyTerm() {
        return this.policyTerm;
    }
    public void setPolicyTerm(String policyTerm) {
        this.policyTerm = policyTerm;
    }
    public String getPremiumPayingTerm() {
        return this.premiumPayingTerm;
    }
    public void setPremiumPayingTerm(String premiumPayingTerm) {
        this.premiumPayingTerm = premiumPayingTerm;
    }
    public Boolean getIsForWholeLife() {
        return this.isForWholeLife;
    }
    public void setIsForWholeLife(Boolean isForWholeLife) {
        this.isForWholeLife = isForWholeLife;
    }
    public String getTotalSumAssured() {
        return this.totalSumAssured;
    }
    public void setTotalSumAssured(String totalSumAssured) {
        this.totalSumAssured = totalSumAssured;
    }
    public String getPlanOption() {
        return this.planOption;
    }
    public void setPlanOption(String planOption) {
        this.planOption = planOption;
    }
    public String getComboPolicyTerm() {
        return this.comboPolicyTerm;
    }
    public void setComboPolicyTerm(String comboPolicyTerm) {
        this.comboPolicyTerm = comboPolicyTerm;
    }
    public String getComboPremiumFrequency() {
        return this.comboPremiumFrequency;
    }
    public void setComboPremiumFrequency(String comboPremiumFrequency) {
        this.comboPremiumFrequency = comboPremiumFrequency;
    }
    public String getComboPremiumPayingTerm() {
        return this.comboPremiumPayingTerm;
    }
    public void setComboPremiumPayingTerm(String comboPremiumPayingTerm) {
        this.comboPremiumPayingTerm = comboPremiumPayingTerm;
    }
    public String getComboTotalSumAssured() {
        return this.comboTotalSumAssured;
    }
    public void setComboTotalSumAssured(String comboTotalSumAssured) {
        this.comboTotalSumAssured = comboTotalSumAssured;
    }
    public String getComboPremiumAmount() {
        return this.comboPremiumAmount;
    }
    public void setComboPremiumAmount(String comboPremiumAmount) {
        this.comboPremiumAmount = comboPremiumAmount;
    }
    public String getTotalCover() {
        return this.totalCover;
    }
    public void setTotalCover(String totalCover) {
        this.totalCover = totalCover;
    }
    public String getTotalPremium() {
        return this.totalPremium;
    }
    public void setTotalPremium(String totalPremium) {
        this.totalPremium = totalPremium;
    }
    public String getInsuredType() {
        return this.insuredType;
    }
    public void setInsuredType(String insuredType) {
        this.insuredType = insuredType;
    }
    public String getDiscoveryFund() {
        return this.discoveryFund;
    }
    public void setDiscoveryFund(String discoveryFund) {
        this.discoveryFund = discoveryFund;
    }
    public String getOpportunitiesFund() {
        return this.opportunitiesFund;
    }
    public void setOpportunitiesFund(String opportunitiesFund) {
        this.opportunitiesFund = opportunitiesFund;
    }
    public String getEquityPlusFund() {
        return this.equityPlusFund;
    }
    public void setEquityPlusFund(String equityPlusFund) {
        this.equityPlusFund = equityPlusFund;
    }
    public String getDiversifiedEquityFund() {
        return this.diversifiedEquityFund;
    }
    public void setDiversifiedEquityFund(String diversifiedEquityFund) {
        this.diversifiedEquityFund = diversifiedEquityFund;
    }
    public String getBlueChipFund() {
        return this.blueChipFund;
    }
    public void setBlueChipFund(String blueChipFund) {
        this.blueChipFund = blueChipFund;
    }
    public String getBalancedFund() {
        return this.balancedFund;
    }
    public void setBalancedFund(String balancedFund) {
        this.balancedFund = balancedFund;
    }
    public String getBondFund() {
        return this.bondFund;
    }
    public void setBondFund(String bondFund) {
        this.bondFund = bondFund;
    }
    public String getLiquidFund() {
        return this.liquidFund;
    }
    public void setLiquidFund(String liquidFund) {
        this.liquidFund = liquidFund;
    }
    public String getInvestmentStyle() {
        return this.investmentStyle;
    }
    public void setInvestmentStyle(String investmentStyle) {
        this.investmentStyle = investmentStyle;
    }
    public String getSourceFund() {
        return this.sourceFund;
    }
    public void setSourceFund(String sourceFund) {
        this.sourceFund = sourceFund;
    }
    public String getTargetFund() {
        return this.targetFund;
    }
    public void setTargetFund(String targetFund) {
        this.targetFund = targetFund;
    }
    public String getTransferAmt() {
        return this.transferAmt;
    }
    public void setTransferAmt(String transferAmt) {
        this.transferAmt = transferAmt;
    }
    public String getTransferDate() {
        return this.transferDate;
    }
    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }
    public String getAgent_code() {
        return this.agent_code;
    }
    public void setAgent_code(String agent_code) {
        this.agent_code = agent_code;
    }
    public String getSource_code() {
        return this.source_code;
    }
    public void setSource_code(String source_code) {
        this.source_code = source_code;
    }
    public String getPremiumPayingTermOption() {
        return this.premiumPayingTermOption;
    }
    public void setPremiumPayingTermOption(String premiumPayingTermOption) {
        this.premiumPayingTermOption = premiumPayingTermOption;
    }
    public String getModifiedDate() {
        return this.modifiedDate;
    }
    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    public String getIsComboAdded() {
        return this.isComboAdded;
    }
    public void setIsComboAdded(String isComboAdded) {
        this.isComboAdded = isComboAdded;
    }
    public Boolean getIsValid() {
        return this.isValid;
    }
    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }
    public String getTxn_id() {
        return this.txn_id;
    }
    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }
    public String getApplicationNumber() {
        return this.applicationNumber;
    }
    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }
    public String getPaymentMode() {
        return this.paymentMode;
    }
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
    
}
